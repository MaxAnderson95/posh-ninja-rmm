#Authorization and Authentication to API Cmdlets

Function Get-NinjaTimeStamp {

    <#

    .SYNOPSIS
        Creates a timestamp for NinajRMM API requests

    .DESCRIPTION
        Creates an RFC 2616 compliant timestamp that is used in the NinjaRMM API requests

    .EXAMPLE
        PS C:> Get-NinjaTimeStamp
        Wed, 12 Apr 2017 13:52:36 GMT

    #>

    Begin
    {

    }

    Process
    {
        $Date = Get-Date -date $(Get-Date).ToUniversalTime() -Format r
    }

    End
    {
        Write-Output $Date
    }

}

Function New-NinjaAuthroization {

    <#

    .SYNOPSIS
        Generates the HMACSHA1 Signature for the API Request

    .DESCRIPTION
        Takes the requested portion of the API request as a string and the public and private keys and uses HMACSHA1 to generate a signature and then generates an Authorization to use in the HTTP request.

    .EXAMPLE
        PS C:> $String = "GET`n`n`nSun, 01 May 2016 06:51:10 GMT`n/v1/customers"
        PS C:> $AccessKeyID = "TF4STGMDR4H7AEXAMPLE"
        PS C:> $SecretAccessKey = "eh14c4ngchhu6283he03j6o7ar2fcuca0example"
        PS C:> New-NinajAuthroization -StringToSign $String -AccessKeyID $AccessKeyID -SecretAccessKey $SecretAccessKey
        NJ TF4STGMDR4H7AEXAMPLE:rEZWuXR0X1wX3autLTHIl2zX98I=

    #>

    PARAM
    (
        [Parameter(Position=0,Mandatory=$True)]
        [String]$StringToSign,

        [Parameter(Position=1,Mandatory=$True)]
        [String]$AccessKeyID,

        [Parameter(Position=2,Mandatory=$True)]
        [String]$SecretAccessKey
    )

    Begin
    {

    }

    Process
    {
        #Convert the String To Sign to a Base64 String
        $StringToSignBase64 = [Convert]::ToBase64String([Text.Encoding]::UTF8.GetBytes($StringToSign))

        #Generate HMACSHA1 Hash
        $HMACSHA = New-Object System.Security.Cryptography.HMACSHA1
        $HMACSHA.Key = [Text.Encoding]::ASCII.GetBytes($SecretAccessKey)
        $Signature = $HMACSHA.ComputeHash([Text.Encoding]::UTF8.GetBytes($StringToSignBase64))
        $Signature = [Convert]::ToBase64String($Signature)

        #Generate the Authorization string
        $Authorization = "NJ $AccessKeyID`:$Signature"

        #Output it
        Write-Output $Authorization
    }

    End
    {

    }

}

Function New-NinjaRequestHeader {

    PARAM
    (
        [Parameter(Mandatory=$True)]
        [string]$HTTPVerb,

        [Parameter]
        [string]$ContentMD5,

        [Parameter]
        [string]$ContentType,

        [Parameter(Mandatory=$True)]
        [string]$Resource,

        [Parameter(Mandatory=$True)]
        [String]$AccessKeyID,

        [Parameter(Mandatory=$True)]
        [String]$SecretAccessKey
    )

    Begin
    {

    }

    Process
    {
        #Generate a Date Stamp
        $Date = Get-NinjaTimeStamp
        
        #Generate the String by concatenating the inputs together
        $StringToSign = $HTTPVerb + "`n" + $ContentMD5 + "`n" + $ContentType + "`n" + $Date + "`n" + $Resource

        $Authorization = New-NinjaAuthroization -StringToSign $StringToSign -AccessKeyID $AccessKeyID -SecretAccessKey $SecretAccessKey

        $Header = @{"Authorization" = $Authorization; "Date" = $Date}

        Write-Output $Header
    }

    End
    {

    }

}

#Authorization and Authentication to API Cmdlets

#API Key Cmdlets

Function Get-NinjaAPIKeys {

    PARAM
    (

    )

    Begin
    {

    }

    Process
    {
        $AccessKeyID = Get-ItemProperty -Path "HKLM:\SOFTWARE\PoSHNinjaRMM\" -Name "AccessKeyID" -ErrorAction SilentlyContinue | Select-Object -ExpandProperty AccessKeyID
        $SecretAccessKey = Get-ItemProperty -Path "HKLM:\SOFTWARE\PoSHNinjaRMM\" -Name "SecretAccessKey" -ErrorAction SilentlyContinue | Select-Object -ExpandProperty SecretAccessKey
    }

    End
    {
        If ($AccessKeyID -eq $Null -or $SecretAccessKey -eq $Null) {
            Write-Error "The Ninja API keys not set in registry, use Set-NinjaKeys to set them"
        }
        
        Else {
            Write-Output @{"AccessKeyID" = $AccessKeyID; "SecretAccessKey" = $SecretAccessKey}
        }
    }

}

Function Set-NinjaAPIKeys {

    PARAM
    (
        [Parameter(Mandatory=$True)]
        [String]$AccessKeyID,

        [Parameter(Mandatory=$True)]
        [String]$SecretAccessKey
    )

    Begin
    {
        
    }

    Process
    {
        New-Item -Path "HKLM:\SOFTWARE\" -Name "PoSHNinjaRMM" -Force
        New-ItemProperty -Path "HKLM:\SOFTWARE\PoSHNinjaRMM" -Name "AccessKeyID" -Value $AccessKeyID -Force
        New-ItemProperty -Path "HKLM:\SOFTWARE\PoSHNinjaRMM" -Name "SecretAccessKey" -Value $SecretAccessKey -Force
    }

    End
    {

    }

}

Function Remove-NinjaApiKeys {

    PARAM
    (
        [Switch]$Force
    )

    Begin
    {
        
    }

    Process
    {
        If (!$Force) 
        {
        
            $title = $Null
            $message = "Delete Ninja API keys from Registry?"
            
            $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "API Keys WILL be deleted from the Registry"
            $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "API keys will NOT be deleted from the Registry"

            $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)

            $result = $host.ui.PromptForChoice($title, $message, $Options, 0)

            Switch ($result)
                {
                    0 { Remove-Item -Path "HKLM:\Software\PoSHNinjaRMM" -Force -ErrorAction SilentlyContinue }
                    1 { Write-Output "Did NOT remove API keys." }
                }
        }

        If ($Force) 
        {
            Remove-Item -Path "HKLM:\Software\PoSHNinjaRMM" -Force -ErrorAction SilentlyContinue
        }
    }

    End
    {

    }

}

#API Key Cmdlets

#API Query Cmdlets

Function Get-NinjaCustomer {

    <#

    .SYNOPSIS
        

    .DESCRIPTION
        

    .EXAMPLE
        

    #>

    PARAM
    (
        [Int]$CustomerID
    )

    Begin
    {
        #Define the AccessKeyID and SecretAccessKeys
        Try 
        {
        $Keys = Get-NinjaAPIKeys
        } 
        
        Catch 
        {
            Throw $Error
        }
        
        #Determine if the input is CustomerID or no Customer entered (looking for list of all)
        If ($CustomerID) {
            $Choice = "CustomerID"
        }
        else {
            $Choice = "None"
        }
    }
    

    Process
    {
        Switch ($Choice) {
            "CustomerID" {
                $Header = New-NinjaRequestHeader -HTTPVerb GET -Resource /v1/customers/$CustomerID -AccessKeyID $Keys.AccessKeyID -SecretAccessKey $Keys.SecretAccessKey

                $Rest = Invoke-RestMethod -Method GET -Uri "https://api.ninjarmm.com/v1/customers/$CustomerID" -Headers $Header
            }

            "None" {
                $Header = New-NinjaRequestHeader -HTTPVerb GET -Resource /v1/customers -AccessKeyID $Keys.AccessKeyID -SecretAccessKey $Keys.SecretAccessKey

                $Rest = Invoke-RestMethod -Method GET -Uri "https://api.ninjarmm.com/v1/customers" -Headers $Header
            }
        }

        Write-Output $Rest
    }

    End
    {

    }

}

#API Query Cmdlets